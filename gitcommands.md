git clone https://lcharoen@bitbucket.org/lcharoen/luckteaching.git/wiki
git pull --all
git add a.txt
git commit -m 'message'


git init
echo Change me > change-me
echo Delete me > delete-me
git add change-me delete-me
git commit -m initial

echo OK >> change-me
rm delete-me
echo Add me > add-me

git status
// Changed but not updated:
//   modified:   change-me
//   deleted:    delete-me
// Untracked files:
//   add-me

git add .
git status

# Changes to be committed:
#   new file:   add-me
#   modified:   change-me
# Changed but not updated:
#   deleted:    delete-me

git reset

git add -u
git status

# Changes to be committed:
#   modified:   change-me
#   deleted:    delete-me
# Untracked files:
#   add-me
