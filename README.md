# CDH Spark #

Spark program codes written in Scala and run on CDH cluster.

## CDH configurations ##
* HDFS location is **cdh3:8020** -- *val filename = "hdfs://cdh3:8020/user/hdfs/luckdata/README.md"*

* Spark Master is at **cdh3:7077** -- *spark://cdh3.mfu.ac.th:7077*

# Example of Word Count -- (as in "wc" directory) #
## Run commands ##
```
#!text
* ~/sbt/bin/sbt package        

* spark-submit --class "wc1" --master spark://cdh3.mfu.ac.th:7077 target/scala-2.10/word-count-on-cdh_2.10-1.0.jar
```

## SBT file (wc1.sbt) ##
```
#!sbt

name := "word count on cdh"

version := "1.0"

scalaVersion := "2.10.4"

libraryDependencies += "org.apache.spark" %% "spark-core" % "1.0.0"

resolvers += "Akka Repository" at "http://repo.akka.io/releases/"

resolvers += "Cloudera Repository" at "https://repository.cloudera.com/artifactory/cloudera-repos/"
```

## Scala File (wc1.scala) ##
```
#!scala

import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf

object wc1 {
  def main(args: Array[String]) {
    // prepare sc
    val filename = "hdfs://cdh3:8020/user/hdfs/luckdata/README.md" 
    val conf = new SparkConf().setAppName("simple word count on cdh")
    val sc = new SparkContext(conf)

    val data = sc.textFile(filename, 2).cache()
    val wc = data.flatMap(line => line.split(" ")).map(word => (word, 1)).reduceByKey(_ + _)
    wc.saveAsTextFile("hdfs://cdh3:8020/user/hdfs/luckoutput/wc_cdh1")
  }
}
```